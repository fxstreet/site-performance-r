#setwd("T:\BitBucket\site-performance-r")

#options("httr_oauth_cache" = "gs_auth")
#library("googlesheets")
#gs_auth()

suppressPackageStartupMessages(library("dplyr"))

library(RJSONIO)
library(gtools)
library(gpagespeed)
library("googlesheets")
library(data.table)

urls <- c("https://www.fxstreet.com/education",
          "https://www.fxstreet.com/live-video",
          "https://www.fxstreet.com/rates-charts/",
          "https://www.fxstreet.com/rates-charts/rates",
          "https://www.fxstreet.com/rates-charts/chart",
          "https://www.fxstreet.com",
          "https://www.fxstreet.com/analysis",
          "https://www.fxstreet.com/news",
          "https://www.dailyfx.com/",
          "https://www.dailyfx.com/real-time-news",
          "https://www.dailyfx.com/calendar",
          "https://www.dailyfx.com/forex-rates",
          "https://www.dailyfx.com/charts",
          "https://www.dailyfx.com/forex-education",
          "https://www.dailyfx.com/technical-analysis",
          "https://www.forexfactory.com/",
          "https://www.forexfactory.com/news.php?",
          "https://www.forexfactory.com/calendar.php",
          "https://www.forexfactory.com/trades.php",
          "https://www.forexfactory.com/market.php",
          "https://www.investing.com/",
          "https://www.investing.com/news/",
          "https://www.investing.com/analysis/",
          "https://www.investing.com/charts/",
          "https://www.investing.com/economic-calendar/",
          "http://www.forexlive.com/",
          "http://www.forexlive.com/EconomicCalendar",
          "http://technical-analysis.forexlive.com/",
          "http://education.forexlive.com/",
          "https://www.myfxbook.com/",
          "http://www.myfxbook.com/forex-economic-calendar",
          "http://www.myfxbook.com/streaming-forex-news")

types <- c("desktop", "mobile")
type <- "desktop"
applicationId <- "AIzaSyCtRKHw3IoLEeFUlzMDxn0f-kj2sp4uArE"
googleSpreadDocumentName <- "1YSyL0lk54pNw1TOrAAIDD6Xnl6QWPdLuVWTjiNMzu6g"
speedcsv <- gs_key(googleSpreadDocumentName)

cont <- 1

for (url in urls)
{
    result <- NULL;
    columnName <- paste(url, type, sep = "_")
    counter <- 0
    
    today <- format(Sys.time(), "%Y/%m/%d")
    repeat{
      try(result <- speedfinder(url, type, applicationId))
      if(!is.null(result) || counter < 5){
        break
      }
      counter <- counter + 1;
    }
    if(!is.null(result))
    {
      row = data.table(result$SPEED)
      row <- row[,page:=columnName]
      row <- row[,fecha:=today]
      
      
      sheet1csv <- gs_read(speedcsv, ws =1)
      rows_sheet1csv <- nrow(sheet1csv)
      rows_number <- as.numeric(rows_sheet1csv)
      plus2 <- c(2)
      final_row <- sum(plus2, rows_number)
      concatenated_row <- paste("A",final_row, sep = "")
      
      export_result <- gs_edit_cells(speedcsv, ws = 1, input = row, anchor = concatenated_row,col_names = FALSE)
    }
    cont <- cont + 1
}
