setwd("C:/gitprojects")
#The aim of this code is to extract member count of Mailchimp segmenting this data
#by newsletters and follows and report it automatically to Spreadsheets and 
#create some charts on Data Studio in order 



#loading some needed libraries
  library(mailchimpR)
  library("googlesheets")
  library(data.table)
  suppressPackageStartupMessages(library("dplyr"))

#calling API and Key credentials
  insertapikey <- c("6a9797e38efb3cd15eeed2320043d427-us10")
  insert_list_id <- c("efa2c9d1ee")
  
#filtering segments
  allsegments <-
  get_list_segments(apikey = insertapikey,list_id = insert_list_id,count = 1211)
  ides <- c(1:900)

#filtering the elements we want to report
  val <- sapply(ides, function (i) {allsegments[[1]][[i]][1:3]})
  
#traposing table in order to adapt it to the format needed
  transt <- t(val)
  
#converting matrix in data.frame in order to filter it more easily
  frameado2 <- as.data.frame(transt)
  
#subseting some IDs and grouping them
  newsletters <- frameado2[frameado2$id %in% c("105121", "105125", "105117", "105129", "105133"),]
  follows <- frameado2[!(frameado2$id %in% c("105121", "105125", "105117", "105129", "105133", "106141", "106145")),]
  today <- format(Sys.time(), "%Y/%m/%d")
  follows[order(as.data.frame(follows$id)),]

#tranforming dataframe into DataTable in order to add new columns with current date and ordering them easily

#filter for follows
  DTfollows = data.table(follows)
  DTunlisted = DTfollows[, member_count:= unlist(member_count)]
  DTordered = DTunlisted[order(-DTfollows$member_count)]
  DTfollowsfinal = DTordered[,fecha:=today]
  
#filter for Newsletters
  DTnewsletter = data.table(newsletters)
  DTnewslettersfinal <- DTnewsletter[,fecha:=today]
  
#filter for aggretated Follows
  AggregatedFollows <- sum(DTfollowsfinal$member_count)
  DTAggregatedFollows <- data.table(AggregatedFollows)
  AggregatedFollowsfinal <- DTAggregatedFollows[,fecha:=today]
  
#some old code commented. This code is useful to create new document, but no needed for our current pourpose
# ExportNewsletters <- gs_new("NLReporting1", input = newsletters, trim = TRUE)
# ExportFollows <- gs_new("FollowReporting", input = follows, trim = TRUE)
# ExportAll <- gs_new("AllReporting", input = frameado2, trim = TRUE)

#This part of the code do the following:
#1. read existing spreadsheed where we want to export our data
#2. count how many existing rows are in the spreadsheet
#3. Sum +2 to the existing number of rows in order to avoid editing existing data
#4. concatenate the numer of rows to obtain selectedrow where the data will be added

#reading newsletter spreadsheet
  newsletters_rep <- gs_key('1PSiMxD6RMvd3-4Hy0V63U1neNoykCp9o2bHl7a0UxGU')
  leyendo_newsletters_spread <- gs_read(newsletters_rep)
  #count(leyendo_newsletters_spread$id)
  rowsnumber_newsletters <- nrow(leyendo_newsletters_spread)
  numeric_rows_newsletters <- as.numeric(rowsnumber_newsletters)
  augmented_newsletters <- c(2)
  sum(numeric_rows_newsletters, augmented_newsletters)
  final_augmented_newsletters <- sum(numeric_rows_newsletters, augmented_newsletters)
  selectedrow_newsletters <- paste("A",final_augmented_newsletters, sep = "")
 
#reading follows spreadsheet
  follows_rep <- gs_key('1OJai0D0Avr-9XSNTwWTLezKU6V6LEqRyyRuDlAFuODU')
  leyendo_follows_spread <- gs_read(follows_rep)
  #count(leyendo_follows_spread$id)
  rowsnumber_follows <- nrow(leyendo_follows_spread)
  numeric_rows_follows <- as.numeric(rowsnumber_follows)
  augmented_follows <- c(2)
  sum(numeric_rows_follows, augmented_follows)
  final_augmented_follows <- sum(numeric_rows_follows, augmented_follows)
  selectedrow_follows <- paste("A",final_augmented_follows, sep = "")
  
#reading aggregated follows spreadsheet
  ag_follows_rep <- gs_key('1mD5mMOBAkZzSOft8NVAL7ocgvqVMUMWFwE6nxPjrGiM')
  leyendo_ag_follows_spread <- gs_read(ag_follows_rep)
  #count(leyendo_ag_follows_spread$id)
  rowsnumber_ag_follows <- nrow(leyendo_ag_follows_spread)
  numeric_rows_ag_follows <- as.numeric(rowsnumber_ag_follows)
  augmented_ag_follows <- c(2)
  sum(numeric_rows_ag_follows, augmented_ag_follows)
  final_augmented_ag_follows <- sum(numeric_rows_ag_follows, augmented_ag_follows)
  selectedrow_ag_follows <- paste("A",final_augmented_ag_follows, sep = "")
  
#exporting data to spreadsheet
  export_follows_ag <- gs_edit_cells(ag_follows_rep, input = AggregatedFollowsfinal, anchor = selectedrow_ag_follows, trim = TRUE)
  export_follows <- gs_edit_cells(follows_rep, input = head(DTfollowsfinal, 20), anchor = selectedrow_follows, trim = TRUE)
  export_newsletters <- gs_edit_cells(newsletters_rep, input = DTnewslettersfinal, anchor = selectedrow_newsletters, trim = TRUE)
  
  
  
  
  